<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
    return;
}
?>

<div id="comments" class="comments-area comment-box">

    <?php
    // You can start editing here -- including this comment!
    if (have_comments()) :
        $comments_number = get_comments_number();
        ?>
        <h3 class="comments-title">Comments on this post</h3>

        <div class="space-10"></div>
        <h6><?php echo !empty($comments_number) ? $comments_number : 0; ?> Comments</h6>

        <div class="comment-list">
            <?php wp_list_comments('type=comment&callback=blogs_comment&style=div&avatar_size=40'); ?>
        </div>

        <?php the_comments_pagination(array(
        'prev_text' => previous_comments_link(),
        'next_text' => next_comments_link(),
    ));

    endif; // Check for have_comments().

    // If comments are closed and there are comments, let's leave a little note, shall we?
    if (!comments_open() && get_comments_number() && post_type_supports(get_post_type(), 'comments')) : ?>

        <p class="no-comments"><?php _e('Comments are closed.', 'twentyseventeen'); ?></p>
        <?php
    endif;

    global $post;

    $comment_args = array('title_reply' => 'post your comment:',
        'fields' => apply_filters('comment_form_default_fields', array(

            'author' => '<div class="col-xs-12 col-sm-6 col-md-4"><p class="comment-form-author">' . '<label for="author">' . __('Author') . '</label> ' . ($req ? '<span>*</span>' : '') .

                '<input id="author" name="author" class="form-control" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" maxlength="245"' . $aria_req . ' /></p></div>',

            'email' => '<div class="col-xs-12 col-sm-6 col-md-4"><p class="comment-form-email">' .

                '<label for="email">' . __('Email') . '</label> ' .

                ($req ? '<span>*</span>' : '') .

                '<input id="email" class="form-control" name="email" ' . ($html5 ? 'type="email"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' />' . '</p></div>',

            'url' => '<div class="col-xs-12 col-sm-6 col-md-4"><p class="comment-form-url">' .

                '<label for="url">' . __('Website') . '</label> ' .

                '<input id="url" class="form-control" name="url"  ' . ($html5 ? 'type="url"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_url']) . '" size="30" />' . '</p></div>'
        )),
        'comment_field' => ' <div class="col-xs-12"><p>' .

            '<label for="comment">' . __('') . '</label>' .

            '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="Message"></textarea>' .

            '</p></div>',

        'comment_notes_after' => '',
        'name_submit' => 'Post comment',
        'submit_button' => ' <div class="col-xs-12"><button type="submit" class="btn btn-link">Post comment</button></div>',

    );
    ?>
    <div class="row">
        <?php comment_form($comment_args, $post->ID); ?>
    </div>
</div>
