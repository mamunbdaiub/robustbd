<?php get_header(); ?>

    <div class="inner-container">
        <!--Header-Area-->
        <?php echo get_template_part('templates/header_tpl', 'none'); ?>
        <!--Header-Area/-->
    </div>

    <div class="main margin-top-20">
        <div class="container">
            <div class="row margin-bottom-40">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        <h2 class="text-uppercase"><?php the_title(); ?></h2>
                        <div class="entry">
                            <?php the_content(); ?>
                        </div>
                    </div>
                <?php endwhile; endif; ?>

            </div>
        </div>
    </div>

    <!-- BEGIN FOOTER -->
<?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>