<?php
/*
 * Template Name: Contact
 */
get_header();
?>

    <div class="inner-container">
        <!--Header-Area-->
        <?php echo get_template_part('templates/header_tpl', 'none'); ?>
        <!--Header-Area/-->
    </div>

    <div class="main margin-top-20">
        <div class="container">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12">
                <div class="content-page padding-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="maps" class="gmaps margin-bottom-40" style="height:400px;"></div>
                        </div>
                        <?php
                        if (have_posts()) : while (have_posts()) : the_post();
                            $address = get_post_meta(get_the_ID(), 'contact_address', true);
                            $email = get_post_meta(get_the_ID(), 'contact_email', true);
                            $contact_facebook_link = get_post_meta(get_the_ID(), 'contact_facebook_link', true);
                            $about = get_post_meta(get_the_ID(), 'contact_about', true);
                            ?>
                            <div class="col-md-9 col-sm-9">
                                <h2 class="tll"><?php the_title(); ?></h2>
                                <?php the_content(); ?>
                            </div>
                            <div class="col-md-3 col-sm-3 sidebar2">
                                <h2 class="tll">Our Contacts</h2>
                                <address>
                                    <?php echo $address; ?>
                                </address>
                                <address>
                                    <strong>Email</strong><br>
                                    <?php echo $email; ?>
                                </address>
                                <ul class="social-icons margin-bottom-40">
                                    <li><a href="<?php echo $contact_facebook_link; ?>" data-original-title="facebook" class="facebook"></a></li>
                                </ul>

                                <h2 class="padding-top-30 tll">About Us</h2>
                                <?php echo $about; ?>
                            </div>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
    </div>

    <!-- BEGIN FOOTER -->
<?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>