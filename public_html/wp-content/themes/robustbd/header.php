<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
    <head>
        <title><?php wp_title('&laquo;', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="author" content="Robust"/>
        <meta name="description" content="Barcode,Printer,Bangladesh,Barcode,Labels,Bangladesh,Barcode,Ribbons,Bangladesh,Barcode,Scanner,Bangladesh,Blitz,2253,Numbering,Machine,Bangladesh,22x12,Numbering,Sticker,Bangladesh"/>
        <meta name="keywords" content="Barcode, Printer, Bangladesh, Barcode, Labels, Bangladesh, Barcode, Ribbons, Bangladesh, Barcode, Scanner, Bangladesh, Blitz, 2253, Numbering, Machine, Bangladesh, 22x12, Numbering, Sticker, Bangladesh"/>

        <!-- Bootstrap core CSS -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <link href="<?php bloginfo('template_url') ?>/resources/css/common.css" rel="stylesheet"/>
        <link href="<?php bloginfo('template_url') ?>/resources/css/style.css" rel="stylesheet"/>
        <link href="<?php bloginfo('template_url') ?>/resources/plugins/bootstrap/css/bootstrap.css" rel="stylesheet"/>
        <link href="<?php bloginfo('template_url') ?>/resources/css/themify-icons.css" rel="stylesheet">

        <!-- Page level plugin styles START -->
        <link href="<?php bloginfo('template_url') ?>/resources/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/resources/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/resources/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
        <!-- Page level plugin styles END -->

        <!-- Theme styles START -->
        <link href="<?php bloginfo('template_url') ?>/resources/frontend/css/components.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/resources/frontend/css/style.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/resources/frontend/css/style-revolution-slider.css" rel="stylesheet"><!-- metronic revo slider styles -->
        <link href="<?php bloginfo('template_url') ?>/resources/frontend/css/style-responsive.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/resources/frontend/css/red.css" rel="stylesheet" id="style-color">
        <link href="<?php bloginfo('template_url') ?>/resources/frontend/css/custom.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url') ?>/resources/frontend/css/portfolio.css" rel="stylesheet">
        <!-- Theme styles END -->

        <script src='https://www.google.com/recaptcha/api.js?render=6Let5okUAAAAAEeenkemlhWzlbb4CTj2t6HLNOy1'></script>

        <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/resources/images/favicon.ico" type="image/x-icon"/>

        <script type="text/javascript">
            var BASEURL = "<?php echo esc_url(get_template_directory_uri()) ?>";
            var IMAGEPATH = "<?php echo esc_url(get_template_directory_uri()); ?>/resources/images/";
        </script>

        <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

        <?php wp_head(); ?>
    </head>