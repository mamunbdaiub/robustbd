<?php get_header(); ?>

    <div class="inner-container">
        <!--Header-Area-->
        <?php echo get_template_part('templates/header_tpl', 'none'); ?>
        <!--Header-Area/-->
    </div>

    <div class="main margin-top-20">
        <div class="container">
            <div class="row margin-bottom-40 content-page">
                <h2 class="tll">Search Results</h2>
                <div class="col-md-12 clearfix no-space">
                    <form class="content-search-view2" action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search..." id="s" name="s">
                            <input type="hidden" name="post_type" value="product" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </span>
                        </div>
                    </form>
                </div>
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="search-result-item">
                            <h4 class="tll"><a href="#"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                            <p> <?php the_excerpt(); ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>

                    <h2>No posts found.</h2>

                <?php endif; ?>
            </div>
        </div>
    </div>

    <!-- BEGIN FOOTER -->
<?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>