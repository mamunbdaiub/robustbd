<div id="sidebar" class="margin-top-20">
    <?php
    if ( ! is_active_sidebar( 'secondary-widget-area' ) ) {
    return;
    }

    // A second sidebar for widgets, just because.
    if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>
        <ul>
            <?php dynamic_sidebar( 'secondary-widget-area' ); ?>
        </ul>

    <?php endif; ?>
</div>