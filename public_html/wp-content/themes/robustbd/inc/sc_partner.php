<?php
add_action('init', 'partners');
function partners()
{
    $labels = array(
        'name' => _x('Partner', 'post type general name'),
        'singular_name' => _x('Partner', 'post type singular name'),
        'add_new' => _x('Add New', 'Partner'),
        'add_new_item' => __('Add New Partner'),
        'edit_item' => __('Edit Partner'),
        'new_item' => __('New Partner'),
        'view_item' => __('View Partner'),
        'search_items' => __('Search Partner'),
        'not_found' => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'rewrite' => array('slug' => 'partner', 'with_front' => false),
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'thumbnail', 'Comments', 'tag'),
        'taxonomies' => array('partner-tag',),
        'has_archive' => true,
    );

    register_post_type('partner', $args);
    flush_rewrite_rules(TRUE);
}

add_action('init', 'partner_tag_taxonomies', 0);

//create two taxonomies, genres and tags for the post type "tag"
function partner_tag_taxonomies()
{
    $labels = array(
        'name' => _x('Tags', 'taxonomy general name'),
        'singular_name' => _x('Tag', 'taxonomy singular name'),
        'search_items' => __('Search Tags'),
        'popular_items' => __('Popular Tags'),
        'all_items' => __('All Tags'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit Tag'),
        'update_item' => __('Update Tag'),
        'add_new_item' => __('Add New Tag'),
        'new_item_name' => __('New Tag Name'),
        'separate_items_with_commas' => __('Separate tags with commas'),
        'add_or_remove_items' => __('Add or remove tags'),
        'choose_from_most_used' => __('Choose from the most used tags'),
        'menu_name' => __('Tags'),
    );

    register_taxonomy('partner_tag', 'partner', array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'partner-tag'),
    ));
}

$prefix = 'partner_';

$meta_box_partner = array(
    'id' => 'partner-meta-box-id',
    'title' => 'Partner Information',
    'page' => 'partner',
    'context' => 'normal',
    'priority' => 'low',
    'fields' => array(
        array(
            'name' => 'Partner Website',
            'desc' => '',
            'id' => $prefix . 'website',
            'type' => 'text'
        )
    )
);

add_action('add_meta_boxes', 'partner_meta_box_add');

function partner_meta_box_add()
{
    global $meta_box_partner;
    add_meta_box($meta_box_partner['id'], $meta_box_partner['title'], 'partner_meta_box_view', $meta_box_partner['page'], $meta_box_partner['context'], $meta_box_partner['priority']);
}

function partner_meta_box_view($post_id)
{
    global $meta_box_partner;
    // Use nonce for verification
    echo '<input type="hidden" name="partner_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';

    echo '<table class="form-table">';
    foreach ($meta_box_partner['fields'] as $field) {
        // get current post meta data
        $meta = get_post_meta($_GET['post'], $field['id'], true);

        if (empty($meta)) $meta = $field['default_value'];

        echo '<tr>';
        echo '<th style="width:20%"><label for="' . $field['id'] . '">' . $field['name'] . ':</label></th>';
        echo '<td>';
        switch ($field['type']) {
            case 'text':
                echo "<table>";
                echo "<tr>";
                echo '<td><input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /></td>';
                echo "<tr>";
                echo "</table>";
                echo '<br /><span class="description">' . $field['desc'] . '</span>';
                break;
            case 'date':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" />
                            <br /><span class="description">' . $field['desc'] . '</span>';
                echo '<script type="text/javascript">
                                jQuery(function() {
                                    jQuery( "#partner_date" ).datepicker({dateFormat: "yy-mm-dd"});
                                  });
                                </script>  ';
                break;
            // textarea
            case 'textarea':
                $args = array("textarea_rows" => 10, "textarea_name" => $field['id'], "editor_class" => "my_editor_custom");
                wp_editor($meta, $field['id'], $args);

                echo '<br /><span class="description">' . $field['desc'] . '</span>';
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="' . $field['id'] . '" id="' . $field['id'] . '" ', $meta ? ' checked="checked"' : '', '/>
                            <label for="' . $field['id'] . '">' . $field['desc'] . '</label>';
                break;
        } //end switch
        echo '</td>';
        echo '</tr>';
    }
    echo '</table>';
}

add_action('save_post', 'partner_meta_box_save');

function partner_meta_box_save($post_id)
{
    global $meta_box_partner;

    // verify nonce
    if (!wp_verify_nonce($_POST['partner_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($meta_box_partner['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}
