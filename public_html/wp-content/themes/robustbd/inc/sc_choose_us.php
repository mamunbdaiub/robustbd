<?php
function sc_choose_us()
{
    $labels = array(
        'name' => _x('Why Choose Us', 'post type general name'),
        'singular_name' => _x('Why Choose Us', 'post type singular name'),
        'add_new' => _x('Add New', 'equipment'),
        'add_new_item' => __('Add New Why Choose Us'),
        'edit_item' => __('Edit New Why Choose Us'),
        'new_item' => __('New Why Choose Us'),
        'all_items' => __('All Why Choose Uss'),
        'view_item' => __('View Why Choose Us'),
        'search_items' => __('Search Why Choose Uss'),
        'not_found' => __('No why choose us found'),
        'not_found_in_trash' => __('No why choose us found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Why Choose Uss'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Holds our why choose us and why choose us specific data',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'comments'),
        'has_archive' => true,
        'rewrite' => array('slug' => 'why choose us', 'with_front' => true),
        'capability_type' => 'post',
        'hierarchical' => false,
        'taxonomies' => array('post_tag')
    );
    register_post_type('choose_us', $args);
    flush_rewrite_rules(TRUE);
}

add_action('init', 'sc_choose_us');

// add meta fields 
$prefix = 'serpiens_';

$meta_box_choose_us = array(
    'id' => 'serpiens-why-choose-us-meta-box-id',
    'title' => 'Why Choose Us Summary Information',
    'page' => 'choose_us',
    'context' => 'normal',
    'priority' => 'low',
    'fields' => array(
        array(
            'name' => 'Short Description',
            'desc' => '',
            'id' => $prefix . 'choose_us_short_description',
            'type' => 'textarea'
        )
    )
);

add_action('add_meta_boxes', 'serpiens_choose_us_meta_box_add');

function serpiens_choose_us_meta_box_add()
{
    global $meta_box_choose_us;
    global $meta_box_choose_us_side;
    add_meta_box($meta_box_choose_us['id'], $meta_box_choose_us['title'], 'serpiens_choose_us_meta_box_view', $meta_box_choose_us['page'], $meta_box_choose_us['context'], $meta_box_choose_us['priority']);
}

function serpiens_choose_us_meta_box_view()
{
    global $meta_box_choose_us, $post_choose_us;
    // Use nonce for verification
    echo '<input type="hidden" name="serpiens_choose_us_meta_box_nonce" value="' . wp_create_nonce(basename(__FILE__)) . '" />';

    echo '<table class="form-table">';
    foreach ($meta_box_choose_us['fields'] as $field) {
        // get current post meta data
        $meta = get_post_meta($_GET['post'], $field['id'], true);
        echo '<tr>';
        echo '<th style="width:20%"><label for="' . $field['id'] . '">' . $field['name'] . ':</label></th>';
        echo '<td>';
        switch ($field['type']) {
            case 'text':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /> 
                            <br /><span class="description">' . $field['desc'] . '</span>';
                break;
            case 'date':
                echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /> 
                            <br /><span class="description">' . $field['desc'] . '</span>';
                echo '<script type="text/javascript">
                                jQuery(function() {
                                    jQuery( "#serpiens_choose_us_date" ).datepicker({dateFormat: "yy-mm-dd"});
                                  });
                                </script>  ';
                break;
            // textarea  
            case 'textarea':
                $args = array("textarea_rows" => 10, "textarea_name" => "serpiens_choose_us_short_description", "editor_class" => "my_editor_custom");
                wp_editor($meta, "serpiens_choose_us_short_description", $args);
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="' . $field['id'] . '" id="' . $field['id'] . '" ', $meta ? ' checked="checked"' : '', '/> 
                            <label for="' . $field['id'] . '">' . $field['desc'] . '</label>';
                break;
        } //end switch          
        echo '</td>';
        echo '</tr>';
    }
    echo '</table>';
}

add_action('save_post', 'serpiens_choose_us_meta_box_save');

function serpiens_choose_us_meta_box_save($post_id)
{
    global $meta_box_choose_us;
    global $post_id;

    // verify nonce
    if (!wp_verify_nonce($_POST['serpiens_choose_us_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($meta_box_choose_us['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

add_action('admin_init', 'choose_us_editor_admin_init');
add_action('admin_head', 'choose_us_editor_admin_head');

function choose_us_editor_admin_init()
{
    wp_enqueue_script('word-count');
    wp_enqueue_script('post');
    wp_enqueue_script('editor');
    wp_enqueue_script('media-upload');
}

function choose_us_editor_admin_head()
{
    wp_tiny_mce();
}

?>