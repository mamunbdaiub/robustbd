<form class="form-inline my-2 my-lg-0" action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" id="s" name="s">
    <input type="hidden" name="post_type" value="product" />
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="searchsubmit">Search</button>
</form>