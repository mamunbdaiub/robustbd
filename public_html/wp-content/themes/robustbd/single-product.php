<?php get_header(); ?>

    <div class="inner-container">
        <!--Header-Area-->
        <?php echo get_template_part('templates/header_tpl', 'none'); ?>
        <!--Header-Area/-->
    </div>

    <div class="main margin-top-20">
        <div class="container">
            <!-- BEGIN CONTENT -->
            <?php if (have_posts()) : while (have_posts()) : the_post();
                $featurePhotoIds = get_post_meta(get_the_ID(), '_easy_image_gallery', true);
                $featurePhotoIdsArray = explode(',', $featurePhotoIds);
                ?>
                <h1><?php the_title(); ?></h1>
                <div class="row margin-bottom-30">
                    <!-- BEGIN CAROUSEL -->
                    <div class="col-md-5 front-carousel">
                        <div class="carousel slide" id="myCarousel" data-ride="carousel">
                            <!-- Carousel items -->
                            <div class="carousel-inner">

                                <?php
                                $i = 0;
                                foreach ($featurePhotoIdsArray as $featurePhotoId) {
                                    $i++;
                                    $fullImagePhoto = wp_get_attachment_image_src($featurePhotoId, 'full', false, '');
                                    ?>
                                    <div class="item carousel-item <?php echo ($i == 1 ? 'active' : ''); ?>">
                                        <img alt="" src="<?php echo $fullImagePhoto[0]; ?>">
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <!-- Carousel nav -->
                            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            </a>
                            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            </a>
                        </div>
                    </div>
                    <!-- END CAROUSEL -->

                    <!-- BEGIN PORTFOLIO DESCRIPTION -->
                    <div class="col-md-7 product-content">
                        <?php
                        echo get_the_content();
                        ?>
                    </div>
                    <!-- END PORTFOLIO DESCRIPTION -->
                </div>
            <?php endwhile; endif; ?>
            <!-- END CONTENT -->
        </div>
    </div>

    <!-- BEGIN FOOTER -->
<?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>