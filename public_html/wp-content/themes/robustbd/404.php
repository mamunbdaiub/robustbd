<?php get_header(); ?>

    <div class="inner-container">
        <!--Header-Area-->
        <?php echo get_template_part('templates/header_tpl', 'none'); ?>
        <!--Header-Area/-->
    </div>

    <div class="main margin-top-20">
        <div class="container">
            <h1><?php the_title(); ?></h1>
            <div class="row margin-bottom-40">
                <h2 style="color: red; font-size: 14px;">Error 404 - Page Not Found</h2>
            </div>
        </div>
    </div>

    <!-- BEGIN FOOTER -->
<?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>