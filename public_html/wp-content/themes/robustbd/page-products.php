<?php
/*
 * Template Name: Product
 */
get_header();
?>
    <div class="inner-container">
        <!--Header-Area-->
        <?php echo get_template_part('templates/header_tpl', 'none'); ?>
        <!--Header-Area/-->
    </div>


    <div class="main margin-top-20">
        <div class="container">
            <!-- BEGIN CONTENT -->
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <h1><?php the_title(); ?></h1>
                <div class="row margin-bottom-40">

                    <?php
                    $productArgs = array(
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'orderby' => 'meta_value',
                        'order' => 'ASC',
                        'posts_per_page' => -1
                    );
                    $i = 0;
                    $j = 0;
                    $products = new WP_Query($productArgs);
                    if (!empty($products->posts)) {
                        $total = $products->post_count;
                        while ($products->have_posts()) : $products->the_post();
                            $featureImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
                            $productSum = get_post_meta(get_the_ID(), 'product_summary', true);
                            ?>
                            <div class="col-md-3 col-sm-4 product-item">
                                <a title="<?php echo get_the_title(get_the_ID()); ?>" href="<?php the_permalink(); ?>">
                                    <img src="<?php echo $featureImg[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive">
                                    <div class="zoomix"><i class="fa fas fa-eye"></i></div>
                                </a>

                                <div class="margin-top-10"></div>
                                <h3><?php the_title(); ?></h3>
                                <div class="margin-top-10"></div>
                                <a href="<?php the_permalink(); ?>" class="btn btn-link">Read more</a>
                            </div>
                            <?php
                        endwhile;
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            <?php endwhile; endif; ?>
            <!-- END CONTENT -->
        </div>
    </div>

    <!-- BEGIN FOOTER -->
<?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>