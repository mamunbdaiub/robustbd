<footer class="black-bg">
    <div class="container">
        <div class="row">
            <div class="offset-top">
                <div class="col-xs-12 col-md-12 col-lg-12 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="well well-lg">
                        <h3>Get in Touch</h3>
                        <div class="space-20"></div>
                        <?php
                        echo do_shortcode("[contact-form-7 id=\"23\" title=\"Contact\"]");
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>