<?php
$args = array('p' => 155, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        ?>
        <div class="header header-mobi-ext" id="header">
            <div class="pre-header">
                <div class="container">
                    <div class="row">
                        <!-- BEGIN TOP BAR LEFT PART -->
                        <div class="col-md-6 col-sm-6 additional-shop-info">
                            <ul class="list-unstyled list-inline">
                                <li><i class="fa fa-phone"></i><span><a href="tel:<?php echo esc_attr(get_option('contact_phone')); ?>"><?php echo esc_attr(get_option('contact_phone')); ?></a></span></li>
                                <li><i class="fa fa-envelope"></i><span><a href="mailto:<?php echo esc_attr(get_option('contact_email')); ?>"><?php echo esc_attr(get_option('contact_email')); ?></a></span></li>
                            </ul>
                        </div>
                        <!-- END TOP BAR LEFT PART -->
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light no-bg">
                <div class="container">
                    <a href="<?php echo site_url(); ?>" class="navbar-brand">
                        <?php
                        $custom_logo_id = get_theme_mod('custom_logo');
                        $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                        if (has_custom_logo()) {
                            echo '<img src="' . esc_url($logo[0]) . '" ' . get_bloginfo('name') . '>';
                        } else {
                            echo '<img src="' . get_bloginfo('template_url') . '/resources/images/robust-logo.png" alt="Robust International Limited"/>';
                        }
                        ?>
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <?php
                        include_once(TEMPLATEPATH . '/lib/Nav_Menu_Walker.php');
                        wp_nav_menu(
                            array(
                                'menu' => 'Header Menu',
                                'container' => FALSE,
                                'container_id' => FALSE,
                                'menu_class' => 'navbar-nav mr-auto',
                                'menu_id' => FALSE,
                                'walker' => new Nav_Menu_Walker
                            )
                        );
                        ?>

                        <?php get_search_form(); ?>
                    </div>
                </div>
            </nav>
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>