<?php
$args = array('p' => 112, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        $featureImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
        ?>
        <div class="choose-us-block content text-center margin-bottom-40 bg-gray" id="<?php echo strtolower(str_replace(' ', '_', get_the_title())); ?>">
            <div class="container">
                <h2><?php echo getTitle(get_the_title()); ?></h2>
                <h4><?php the_content(); ?></h4>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <img src="<?php echo $featureImg[0]; ?>" alt="Why to choose us" class="img-responsive">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <div class="panel-group" id="accordion1">
                            <?php
                            $chooseArgs = array(
                                'post_type' => 'choose_us',
                                'post_status' => 'publish',
                                'orderby' => 'meta_value',
                                'order' => 'ASC',
                                'posts_per_page' => -1
                            );
                            $choose = new WP_Query($chooseArgs);
                            if (!empty($choose->posts)) {
                                $total = $choose->post_count;
                                $i = 0;
                                while ($choose->have_posts()) : $choose->the_post();
                                    $i++;
                                    ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_<?php echo $i; ?>"><?php echo get_the_title(); ?></a>
                                            </h5>
                                        </div>
                                        <div id="accordion1_<?php echo $i; ?>" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                            }
                            wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>