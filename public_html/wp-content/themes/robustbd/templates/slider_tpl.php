<div class="page-slider" id="home">
    <div class="fullwidthbanner-container revolution-slider margin-bottom-0">
        <div class="fullwidthabnner">
            <ul id="revolutionul">

                <?php
                $bannerArgs = array(
                    'post_type' => 'banner',
                    'post_status' => 'publish',
                    'order' => 'ASC',
                    'posts_per_page' => -1
                );
                $j = $i = 0;
                $banners = new WP_Query($bannerArgs);
                if (!empty($banners->posts)) {
                    $item = [];
                    while ($banners->have_posts()) : $banners->the_post();
                        $i++;
                        $featureWorkImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
                        $fields = get_field_objects(get_the_ID());
                        $item[$j]['full_img'] = $featureWorkImg[0];
                        $item[$j]['title'] = get_the_title();
                        $item[$j]['content'] = get_the_content();
                        $item[$j]['sub_title'] = get_field('sub_title', get_the_ID());
                        $item[$j]['link_title'] = get_field('link_title', get_the_ID());
                        $item[$j]['link'] = get_field('link', get_the_ID());
                        $item[$j]['single_image_show'] = get_field('single_image_show', get_the_ID());
                        $item[$j]['multiple_image_show'] = get_field('multiple_image_show', get_the_ID());
                        $item[$j]['multiple_image_big'] = get_field('big_image_multiple', get_the_ID());
                        $item[$j]['multiple_image_small_1'] = get_field('multi_small_image_1', get_the_ID());
                        $item[$j]['multiple_image_small_2'] = get_field('multi_small_image_2', get_the_ID());

                        if (!empty($fields)) {
                            $m = 0;
                            foreach ($fields as $field_name => $field) {
                                if (in_array($field['type'], array('image', 'radio'))) {
                                    if ($field['type'] == 'radio') {
                                        $item[$j]['radio'] = $field['value'];
                                    } elseif ($field['type'] == 'image') {
                                        $item[$j]['images'][$m] = $field['value'];
                                    }
                                }
                                $m++;
                            }
                        }

                        echo displayBanner($item[$j]);
//                         pr($item[$j]);
                        ?>
                        <?php
                        $j++;
                    endwhile;
                }
                wp_reset_postdata();
                ?>
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
</div>