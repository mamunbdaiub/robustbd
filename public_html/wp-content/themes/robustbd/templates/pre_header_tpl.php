<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-6 col-sm-6 additional-shop-info">
                <ul class="list-unstyled list-inline">
                    <li><i class="fa fa-phone"></i><span><a href="tel:<?php echo esc_attr( get_option('contact_phone') ); ?>"><?php echo esc_attr( get_option('contact_phone') ); ?></a></span></li>
                    <li><i class="fa fa-envelope"></i><span><?php echo esc_attr( get_option('contact_email') ); ?></span></li>
                </ul>
            </div>
            <!-- END TOP BAR LEFT PART -->
        </div>
    </div>
</div>