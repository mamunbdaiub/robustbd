<div class="our-partners bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php
                $args = array('p' => 15, 'post_type' => 'page');
                $the_query = new WP_Query($args);

                if ($the_query->have_posts()) {
                    while ($the_query->have_posts()) {
                        $the_query->the_post();
                        ?>
                        <h2 class="tll"><a href="<?php echo the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                        <?php the_content(); ?>
                        <?php
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();
                } else {
                    // no posts found
                }
                ?>
            </div>
            <div class="col-md-9">
                <div class="owl-carousel owl-carousel6-brands">
                    <?php
                    $clientArgs = array(
                        'post_type' => 'partner',
                        'post_status' => 'publish',
                        'orderby' => 'meta_value',
                        'order' => 'ASC',
                        'posts_per_page' => -1
                    );
                    $clients = new WP_Query($clientArgs);
                    if (!empty($clients->posts)) {
                        while ($clients->have_posts()) : $clients->the_post();
                            $featureImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
                            $partner_website = get_post_meta(get_the_ID(), "partner_website", true);
                            ?>
                            <div class="partner-item">
                                <a href="<?php echo !empty($partner_website) ? $partner_website : 'javascript:void(0)' ?>">
                                    <img src="<?php echo $featureImg[0]; ?>" class="img-responsive" alt="<?php the_title(); ?>">
                                    <img src="<?php echo $featureImg[0]; ?>" class="color-img img-responsive" alt="<?php echo 'Hover' . get_the_title(); ?>">
                                </a>
                            </div>
                            <?php
                        endwhile;
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>