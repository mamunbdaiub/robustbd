<?php
$args = array('p' => 13, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        ?>
        <div class="services-block content tlc bg-gray padding-bottom-60" id="<?php echo strtolower(str_replace(' ', '_', get_the_title())); ?>">
            <div class="container">
                <h2><?php echo getTitle(get_the_title()); ?></h2>
                <h4><?php the_content(); ?></h4>

                <?php
                $serviceArgs = array(
                    'post_type' => 'service',
                    'post_status' => 'publish',
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'posts_per_page' => 4
                );
                $services = new WP_Query($serviceArgs);
                if (!empty($services->posts)) {
                    echo "<div class=\"row\">";
                    while ($services->have_posts()) : $services->the_post();
                        $featureImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
                        $serviceSum = get_post_meta(get_the_ID(), 'service_summary', true);
                        ?>
                        <div class="col-md-3 col-sm-3 col-xs-12 item">
                            <div class="service-img">
                                <img src="<?php echo $featureImg[0]; ?>" alt="<?php echo get_the_title(get_the_ID()); ?>" class="img-responsive">
                            </div>
                            <h3><?php echo get_the_title(get_the_ID()); ?></h3>
                            <p><?php echo $serviceSum; ?></p>
                        </div>
                        <?php
                    endwhile;

                    echo "</div>";
                }
                wp_reset_postdata();
                ?>

                <div class="link tlc">
                    <a href="<?php echo get_permalink(13); ?>" class="btn btn-link no-round text-uppercase">View All Services</a>
                </div>
            </div>
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>