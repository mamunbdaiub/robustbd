<?php
$args = array('p' => 9, 'post_type' => 'page');
$the_query = new WP_Query($args);

if ($the_query->have_posts()) {
    while ($the_query->have_posts()) {
        $the_query->the_post();
        ?>
        <div class="product-block content bg-white tlc padding-bottom-60" id="<?php echo strtolower(str_replace(' ', '_', get_the_title())); ?>">
            <div class="container">
                <h2 class="margin-bottom-10">Our <strong>products</strong></h2>
                <h4 class="margin-bottom-50"><?php the_content(); ?></h4>

                <?php
                $productArgs = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'posts_per_page' => 4
                );
                $i = 0;
                $j = 0;
                $products = new WP_Query($productArgs);
                if (!empty($products->posts)) {
                    $total = $products->post_count;
                    while ($products->have_posts()) : $products->the_post();
                        $j++;
                        $i++;
                        if ($i == 1) {
                            echo "<div class='row'>";
                        }

                        $featureImg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full', false, '');
                        $productSum = get_post_meta(get_the_ID(), 'product_summary', true);
                        ?>
                        <div class="item col-md-3 col-sm-6 col-xs-12">
                            <img src="<?php echo $featureImg[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive">
                            <a href="<?php the_permalink(); ?>" class="zoom valign-center">
                                <div class="valign-center-elem">
                                    <strong><?php the_title(); ?></strong>
                                    <em><?php echo $productSum; ?></em>
                                    <b>Details</b>
                                </div>
                            </a>
                        </div>
                        <?php
                        if ($i == 4 || $j == $total) {
                            $i = 0;
                            echo "</div>";
                        }
                    endwhile;
                }
                wp_reset_postdata();
                ?>
                <div class="link tlc">
                    <a href="<?php echo get_permalink(9); ?>" class="btn btn-link no-round text-uppercase">View All Product</a>
                </div>
            </div>
        </div>
        <?php
    }
    /* Restore original Post Data */
    wp_reset_postdata();
} else {
    // no posts found
}
?>