<footer class="black-bg">
    <div id="footer-nav" class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-9">
                    <div id="footer-socials">
                        <div class="social-menu">
                            <a href="https://www.facebook.com/Robust-International-Limited-211462078875253/" target="_blank" title="Facebook"><span class="ti-facebook"></span></a>
                        </div>
                    </div><!-- #footer-socials -->
                </div>

                <div class="col-md-6 col-md-pull-9 tlr">
                    <div class="footer-site-info tlr"><?php echo date('Y'); ?> &copy; Copyrights <?php bloginfo('name'); ?></div>
                </div>
            </div>
        </div>
    </div>
</footer>