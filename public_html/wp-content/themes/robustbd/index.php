<?php get_header(); ?>
    <body>
    <!-- BEGIN TOP BAR -->
<!--    --><?php //get_template_part('templates/pre_header_tpl', 'none'); ?>
    <!-- END TOP BAR -->

    <!-- BEGIN NAVIGATION -->
    <?php get_template_part('templates/header_tpl', 'none'); ?>
    <!-- BEGIN NAVIGATION -->

    <!-- BEGIN SLIDER -->
    <?php get_template_part('templates/slider_tpl', 'none'); ?>
    <!-- END SLIDER -->

    <!-- Services block BEGIN -->
    <?php get_template_part('templates/service_tpl', 'none'); ?>
    <!-- Services block END -->

    <!-- Product block BEGIN -->
    <?php get_template_part('templates/product_tpl', 'none'); ?>
    <!-- Product block END -->

    <!-- Choose us block BEGIN -->
    <?php get_template_part('templates/choose_us_tpl', 'none'); ?>
    <!-- Choose us block END -->

    <!-- BEGIN PARTNERS -->
    <?php get_template_part('templates/partner_tpl', 'none'); ?>
    <!-- END PARTNERS -->

    <!-- BEGIN MAP -->
    <?php get_template_part('templates/map_tpl', 'none'); ?>
    <!-- END MAP -->

    <!-- BEGIN FOOTER -->
    <?php get_template_part('templates/contact_tpl', 'none'); ?>
    <?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>