<?php
/*
 * Template Name: Page With Sidebar
 */
get_header();
?>

    <div class="inner-container">
        <!--Header-Area-->
        <?php echo get_template_part('templates/header_tpl', 'none'); ?>
        <!--Header-Area/-->
    </div>

    <div class="main margin-top-20">
        <div class="container">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12">
                <div class="content-page padding-0">
                    <div class="row">
                        <?php
                        if (have_posts()) : while (have_posts()) : the_post();
                            ?>
                            <div class="col-md-9 col-sm-9">
                                <h2 class="tll"><?php the_title(); ?></h2>
                                <?php the_content(); ?>
                            </div>
                            <div class="col-md-3 col-sm-3 ">
                                <?php get_sidebar(); ?>
                            </div>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
    </div>

    <!-- BEGIN FOOTER -->
<?php get_template_part('templates/footer_tpl', 'none'); ?>
    <!-- END FOOTER -->

<?php get_footer(); ?>