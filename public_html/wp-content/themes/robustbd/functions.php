<?php

require_once(TEMPLATEPATH . "/inc/sc_product.php");
require_once(TEMPLATEPATH . "/inc/sc_choose_us.php");
require_once(TEMPLATEPATH . "/inc/sc_partner.php");
require_once(TEMPLATEPATH . "/inc/sc_service.php");
require_once(TEMPLATEPATH . "/inc/sc_banner.php");

function pr($result)
{
    echo "<pre>";
    var_dump($result);
    echo "</pre>";
}

function has_children()
{
    global $post;
    $pages = get_pages('child_of=' . $post->ID);
    return count($pages);
}

if (class_exists('MultiPostThumbnails')) {

    new MultiPostThumbnails(array(
        'label' => 'Secondary Image',
        'id' => 'secondary-image',
        'post_type' => 'client'
    ));

}

add_theme_support('post-thumbnails');
function shorten_string($string, $wordsreturned)
{
    $retval = $string; //    Just in case of a problem
    $array = explode(" ", $string);

    array_splice($array, $wordsreturned);
    $retval = implode(" ", $array);

    return $retval;
}


// Add RSS links to <head> section
automatic_feed_links();


// Clean up the <head>
function removeHeadLinks()
{
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
}

add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');
// add sidebar widget


function blankpress_widgets_init()
{

    register_sidebar(array(
        'name' => __('Primary Widget Area', 'Blankpress'),
        'id' => 'primary-widget-area',
        'description' => __('The primary widget area', 'Blankpress'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
    register_sidebar(array(
        'name' => __('Secondary Widget Area', 'Blankpress'),
        'id' => 'secondary-widget-area',
        'description' => __('The secondary widget area', 'Blankpress'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

add_action('widgets_init', 'blankpress_widgets_init');

$wnm_custom = array('template_url' => get_bloginfo('template_url'));
wp_localize_script('custom-js', 'wnm_custom', $wnm_custom);

function wpbeginner_numeric_posts_nav()
{

    if (is_singular())
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1)
        return;

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max = intval($wp_query->max_num_pages);

    /**    Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /**    Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /**    Previous Post Link */
    if (get_previous_posts_link())
        printf('<li>%s</li>' . "\n", get_previous_posts_link());

    /**    Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /**    Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array)$links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /**    Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /**    Next Post Link */
    if (get_next_posts_link())
        printf('<li>%s</li>' . "\n", get_next_posts_link());

    echo '</ul></div>' . "\n";
}

function wpb_set_post_views($postID)
{
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//To keep the count accurate, lets get rid of prefetching
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wp_get_cat_postcount($post_type, $taxonomy, $term_id)
{
    //return $count;
    $args = array(
        'post_type' => $post_type, //post type, I used 'product'
        'post_status' => 'publish', // just tried to find all published post
        'posts_per_page' => -1,  //show all
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => $taxonomy,  //taxonomy name  here, I used 'product_cat'
                'field' => 'id',
                'terms' => array($term_id)
            )
        )
    );

    $query = new WP_Query($args);

    return (int)$query->post_count;

}

function blogs_comment($comment, $args, $depth)
{
    if ('div' === $args['style']) {
        $tag = 'div ';
        $add_below = 'media';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    } ?>
    <<?php echo $tag; ?><?php comment_class(empty($args['has_children']) ? 'media ' : 'parent media'); ?> id="comment-<?php comment_ID() ?>"><?php
    if ('div' != $args['style']) { ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
    } ?>
    <div class="media-left">
        <div class="box60">
            <?php
            if ($args['avatar_size'] != 0) {
                echo get_avatar($comment, $args['avatar_size']);
            }
            ?>
        </div>
        <div class="reply" style="text-align: center; margin-top: 10px;">
            <?php
            comment_reply_link(
                array_merge(
                    $args,
                    array(
                        'add_below' => $add_below,
                        'depth' => $depth,
                        'max_depth' => $args['max_depth']
                    )
                )
            );
            ?>
        </div>
        <div class="comment-meta commentmetadata" style="text-align: center; margin-top: 10px;">
            <?php
            edit_comment_link(__('Edit'), '', ''); ?>
        </div>
    </div>
    <div class="media-right">
        <h4><?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()); ?></h4>
        <small><?php printf(_x('%s ago', '%s = human-readable time difference', 'your-text-domain'), human_time_diff(get_comment_time('U'), current_time('timestamp'))); ?></small>
        <div class="space-10"></div>
        <?php comment_text(); ?>
        <div class="space-20"></div>
        <?php
        if ($comment->comment_approved == '0') {
            ?>
            <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.'); ?></em><br/><?php
        }
        ?>
    </div>
    <?php
    if ('div' != $args['style']) :
        ?>
        </div>

        <?php
    endif;
}

function wpa_tag_posts_per_page($query)
{
    if (!is_admin()
        && $query->is_tag()
        && $query->is_main_query()) {
        $query->set('posts_per_page', 10);
    }
}

add_action('pre_get_posts', 'wpa_tag_posts_per_page');

function themename_custom_logo_setup()
{
    $defaults = array(
        'height' => 100,
        'width' => 400,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}

add_action('after_setup_theme', 'themename_custom_logo_setup');

function getTermCategory($terms)
{
    $categoryConcate = '';
    if (!empty($terms)) {
        foreach ($terms as $key => $value) {
            $categoryConcate .= 'category_' . $value . ' ';
        }
    }

    return $categoryConcate;
}

add_action('wp_enqueue_scripts', 'wp_register_javascript', 100);

function wp_register_javascript()
{
    wp_register_script('mediaelement', plugins_url('wp-mediaelement.min.js', __FILE__), array('jquery'), '4.8.2', true);
    wp_enqueue_script('mediaelement');
}

function getString($string, $count)
{
    $returnStr = '';
    if (!empty($string)) {
        for ($i = 0; $i <= $count; $i++) {
            if ($i == $count) {
                break;
            } else {
                $returnStr .= $string[$i];
                continue;
            }
        }
    }

    return $returnStr;
}

function wpdocs_theme_robust_scripts()
{
    wp_enqueue_style('global', get_stylesheet_uri());

    if (is_page(9)) {
        wp_enqueue_style('page-products', get_template_directory_uri() . '/resources/frontend/css/product.css');
    }
}

add_action('wp_enqueue_scripts', 'wpdocs_theme_robust_scripts');

function getTitle($string)
{
    $pieces = explode(" ", $string);
    $title = "";
    if (!empty($pieces)) {
        $total = count($pieces);
        $i = 0;
        foreach ($pieces as $key => $value) {
            $i++;
            if ($key == 0) {
                $title .= $value;
            } else {
                if ($i == 2) {
                    $title .= ' <strong>';
                }

                $title .= $value;

                if ($i == $total) {
                    $title .= '</strong>';
                }
            }

        }
    }
    return $title;
}

function getTitleBanner($string)
{
    $pieces = explode(" ", $string);
    $title = "";
    if (!empty($pieces)) {
        $total = count($pieces);
        $i = 0;
        foreach ($pieces as $key => $value) {
            $i++;
            if ($key == 0) {
                $title .= " " . $value;
            } else {
                if ($i == 4) {
                    $title .= ' <br>';
                }

                $title .= " " . $value;
            }

        }
    }
    return $title;
}

/**
 * @param $post_ID
 */
function restrict_post_deletion($post_ID)
{
    $restricted_pages = array(17, 155, 9, 112);
    if (in_array($post_ID, $restricted_pages)) {
        echo "You are not authorized to delete this page.";
        exit;
    }
}


add_action('trash_post', 'restrict_post_deletion', 10, 1);
add_action('delete_post', 'restrict_post_deletion', 10, 1);
add_action('wp_trash_post', 'restrict_post_deletion');

function contact_add_meta_boxes($post)
{
    global $post;
    if ($post->ID == 17) {
        add_meta_box(
            'contact-address-custom-metabox', // Metabox HTML ID attribute
            'Sidebar Address', // Metabox title
            'contact_page_template_metabox', // callback name
            'page',
            'normal',
            'high'
        );

        add_meta_box(
            'contact-email-custom-metabox', // Metabox HTML ID attribute
            'Sidebar Email', // Metabox title
            'email_page_template_metabox', // callback name
            'page',
            'normal',
            'high'
        );

        add_meta_box(
            'contact-social-custom-metabox', // Metabox HTML ID attribute
            'Sidebar Social Links', // Metabox title
            'social_page_template_metabox', // callback name
            'page',
            'normal',
            'high'
        );

        add_meta_box(
            'contact-about-custom-metabox', // Metabox HTML ID attribute
            'Sidebar About Us', // Metabox title
            'about_page_template_metabox', // callback name
            'page',
            'normal',
            'high'
        );
    }
}

// Make sure to use "_" instead of "-"
add_action('add_meta_boxes_page', 'contact_add_meta_boxes');


function contact_page_template_metabox()
{
    global $post;
    $address = get_post_meta($post->ID, 'contact_address', true);
    wp_editor($address, 'contact_address', $settings = array('textarea_rows' => 20));
}

function email_page_template_metabox()
{
    global $post;
    $email = get_post_meta($post->ID, 'contact_email', true);
    wp_editor($email, 'contact_email', $settings = array('textarea_rows' => 20));
}

function social_page_template_metabox()
{
    global $post;
    $contact_facebook_link = get_post_meta($post->ID, 'contact_facebook_link', true);
    ?>
    <table>
        <tr>
            <td>Facebook</td>
            <td><input style="width: 100%;" type="text" name="contact_facebook_link" value="<?php echo !empty($contact_facebook_link) ? $contact_facebook_link : ''; ?>"></td>
        </tr>
    </table>

    <?php
}


function about_page_template_metabox()
{
    global $post;
    $about = get_post_meta($post->ID, 'contact_about', true);
    wp_editor($about, 'contact_about', $settings = array('textarea_rows' => 20));
}

add_action('save_post', 'contact_save_custom_post_meta', 10, 2);

function contact_save_custom_post_meta($post_id)
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    // First we need to check if the current user is authorised to do this action.
    if (!current_user_can('edit_post', $post_id))
        return;

    $contact_address = $_POST['contact_address'];
    $contact_email = $_POST['contact_email'];
    $contact_facebook_link = $_POST['contact_facebook_link'];
    $contact_about = $_POST['contact_about'];

    // Do something with $mydata
    // either using
    update_post_meta($post_id, 'contact_address', $contact_address);
    update_post_meta($post_id, 'contact_email', $contact_email);
    update_post_meta($post_id, 'contact_facebook_link', $contact_facebook_link);
    update_post_meta($post_id, 'contact_about', $contact_about);
}

function displayBanner($item)
{
    $html = '';

    if (!empty($item)) {
        if ($item['single_image_show'] == 'Yes') {
            $html .= getSingleImageSlider($item);
        } elseif ($item['multiple_image_show'] == 'Yes') {
            $html .= getMultiImageSlider($item);
        }
    }
    return $html;
}

function getSingleImageSlider($item)
{
    $html = '';

    $html .= '<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400">';
    $html .= '<img src="' . get_bloginfo('template_url') . '/resources/frontend/img/revolutionslider/bg1.jpg" alt="">';
    $html .= '<div class="caption lft slide_title slide_item_left"
                           data-x="30"
                            data-y="90"
                            data-speed="400"
                            data-start="1500"
                            data-easing="easeOutExpo">
                        ' . getTitleBanner($item["title"]) . '
                    </div>';
    $html .= '<div class="caption lft slide_subtitle slide_item_left"
                             data-x="30"
                            data-y="245"
                            data-speed="400"
                            data-start="2000"
                            data-easing="easeOutExpo">
                        ' . $item["sub_title"] . '
                    </div>';
    $html .= '<div class="caption lft slide_desc slide_item_left"
               data-x="30"
                data-y="285"
                data-speed="400"
                data-start="2500"
                data-easing="easeOutExpo">
                        ' . shorten_string($item["content"], 20) . '
                    </div>';
    $html .= '<a class="caption lft btn dark slide_btn slide_item_left" href="' . $item['link'] . '"
           data-x="30"
           data-y="355"
           data-speed="400"
           data-start="3000"
           data-easing="easeOutExpo">
              ' . $item["link_title"] . '
        </a>
        <div class="caption lfb"
             data-x="640"
             data-y="0"
             data-speed="700"
             data-start="1000"
             data-easing="easeOutExpo">';
    $html .= '<img src="' . $item['images'][0]['url'] . '" alt="' . $item['title'] . '">
        </div>
    </li>';
    return $html;

}

function getMultiImageSlider($item)
{
    $html = '';
    $html .= '<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400" data-thumb="<?php bloginfo(\'template_url\') ?>/resources/frontend/img/revolutionslider/thumbs/thumb2.jpg">';
    $html .= '<img src="' . get_bloginfo('template_url') . '/resources/frontend/img/revolutionslider/bg1.jpg" alt="">';
    $html .= '<div class="caption lfl slide_title slide_item_left"
                            data-x="30"
                            data-y="90"
                            data-speed="400"
                            data-start="1500"
                            data-easing="easeOutExpo">
                       ' . getTitleBanner($item["title"]) . '
                    </div>';
    $html .= '<div class="caption lfl slide_subtitle slide_item_left"
                             data-x="30"
                            data-y="225"
                            data-speed="400"
                            data-start="2000"
                            data-easing="easeOutExpo">
                        ' . $item["sub_title"] . '
                    </div>';
    $html .= '<div class="caption lfl slide_desc slide_item_left"
                         data-x="30"
                         data-y="285"
                         data-speed="400"
                         data-start="4500"
                         data-easing="easeOutExpo">
                        ' . shorten_string($item["content"], 20) . '
                    </div>';

    $html .= '<a class="caption lft btn dark slide_btn slide_item_left" href="' . $item['link'] . '"
           data-x="30"
           data-y="355"
           data-speed="400"
           data-start="3000"
           data-easing="easeOutExpo">
              ' . $item["link_title"] . '
        </a>';

    $html .= '<div class="caption lfr slide_item_right"
               data-x="635"
                         data-y="105"
                         data-speed="1200"
                         data-start="1500"
                         data-easing="easeOutBack">'
        . (!empty($item["multiple_image_big"]["url"]) ? '<img src="' . $item["multiple_image_big"]["url"] . '" alt="Image 1">' : "") . '
                    </div>
                    <div class="caption lfr slide_item_right"
                         data-x="580"
                         data-y="245"
                         data-speed="1200"
                         data-start="2000"
                         data-easing="easeOutBack">'
        . (!empty($item["multiple_image_small_1"]["url"]) ? '<img src="' . $item["multiple_image_small_1"]["url"] . '" alt="Image 1">' : "") . '
                    </div>
                    <div class="caption lfr slide_item_right"
                         data-x="835"
                         data-y="230"
                         data-speed="1200"
                         data-start="3000"
                         data-easing="easeOutBack">'
        . (!empty($item["multiple_image_small_2"]["url"]) ? '<img src="' . $item["multiple_image_small_2"]["url"] . '" alt="Image 1">' : "") . '
                    </div>
                </li>
    ';

    return $html;
}

// create custom plugin settings menu
add_action('admin_menu', 'extra_plugin_create_menu');

function extra_plugin_create_menu() {

    //create new top-level menu
    add_menu_page('Common Plugin Settings', 'Common Settings', 'administrator', __FILE__, 'extra_plugin_settings_page' , plugins_url('/images/icon.png', __FILE__) );

    //call register settings function
    add_action( 'admin_init', 'register_extra_plugin_settings' );
}


function register_extra_plugin_settings() {
    //register our settings
    register_setting( 'extra-plugin-settings-group', 'contact_email' );
    register_setting( 'extra-plugin-settings-group', 'contact_phone' );
}

function extra_plugin_settings_page() {
    ?>
    <div class="wrap">
        <h1>Your Plugin Name</h1>

        <form method="post" action="options.php">
            <?php settings_fields( 'extra-plugin-settings-group' ); ?>
            <?php do_settings_sections( 'extra-plugin-settings-group' ); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Contact Email</th>
                    <td><input type="text" name="contact_email" value="<?php echo esc_attr( get_option('contact_email') ); ?>" /></td>
                </tr>

                <tr valign="top">
                    <th scope="row">Contact Phone</th>
                    <td><input type="text" name="contact_phone" value="<?php echo esc_attr( get_option('contact_phone') ); ?>" /></td>
                </tr>
            </table>

            <?php submit_button(); ?>

        </form>
    </div>
<?php } ?>
