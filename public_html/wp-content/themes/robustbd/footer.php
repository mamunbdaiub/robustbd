<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url') ?>/resources/js/respond.min.js"></script>
<![endif]-->

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/resources/js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/resources/plugins/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url') ?>/resources/plugins/fancybox/source/jquery.fancybox.pack.js"></script><!-- pop up -->
<script src="<?php bloginfo('template_url') ?>/resources/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

<!-- BEGIN RevolutionSlider -->
<script src="<?php bloginfo('template_url') ?>/resources/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ?>/resources/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ?>/resources/frontend/scripts/revo-slider-init.js" type="text/javascript"></script>
<!-- END RevolutionSlider -->

<script src="<?php bloginfo('template_url') ?>/resources/layout/js/jquery.easing.js"></script>
<script src="<?php bloginfo('template_url') ?>/resources/layout/js/jquery.parallax.js"></script>
<script src="<?php bloginfo('template_url') ?>/resources/layout/js/jquery.scrollTo.min.js"></script>

<script src="<?php bloginfo('template_url') ?>/resources/layout/js/layout.js" type="text/javascript"></script>

<script src="<?php bloginfo('template_url') ?>/resources/onepage/scripts/revo-ini.js" type="text/javascript"></script>
<!--<script src="--><?php //bloginfo('template_url') ?><!--/resources/onepage/scripts/jquery.nav.js"></script>-->

<!--Maps JS-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCS-JHIeMkdKVBkFe8PTxyovKpZ3-JMYfM&sensor=false"></script>
<script src="<?php bloginfo('template_url') ?>/resources/js/maps.js"></script>
<script src="<?php bloginfo('template_url') ?>/resources/onepage/scripts/layout-one-page.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        Layout.init();
        LayoutOnePage.init();
        Layout.initOWL();
        RevosliderInit.initRevoSlider();
        Layout.initTwitter();

        $("#menu-item-173").on("click", function (e) {
            var body = $("html, body");
            body.stop().animate({scrollTop: 0}, 500, 'swing', function () {
            });

        });

        $(document).load().scrollTop(0);

        $(document).ready(function () {
            $(document).scroll(function () {
                var scroll = $(this).scrollTop();
                var topDist = $(".header").position();
                var outerHeight = $('.header').outerHeight();
                if (scroll > outerHeight) {
                    $(".header").addClass("fixNav");
                    $('.header').css({"position": "fixed", "top": "0", "transition": "all 1s ease-in", "z-index": "9", "bottom": "auto"});
                } else {
                    $(".header").removeClass("fixNav");
                    $('.header').css({"position": "relative", "top": "0", "transition": "all 1s ease-in"});
                }
            });
        });
    });

    grecaptcha.ready(function() {
        grecaptcha.execute('6Let5okUAAAAAEeenkemlhWzlbb4CTj2t6HLNOy1', {action: 'action_name'})
            .then(function(token) {
// Verify the token on the server.
            });
    });
</script>


<?php wp_footer(); ?>
</body>
</html>