<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'robustbd');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kE6T4!y=daEuV? [x2_YZ&3h`@%pnIU8lWmGI}|O$4^+]ICO]p6@k0nA]X </ec ');
define('SECURE_AUTH_KEY',  '%;z )4O+*ZNH.4+5N %2W9-d.riv[*IdH(-?z]1,Q*X/lhoP#vL0gp33pz| Z yC');
define('LOGGED_IN_KEY',    '*JS@BNS`+E,z`!*GRPRvE^o$ylR%:zcg6 H*^exsvr>lj4]1 E2[!IMp-4Mg)sn9');
define('NONCE_KEY',        'dN``4}**{I^{Z5gc6K},&s&OtS{bG_VTUs|$_UE,B,@f?Y %<<]v ghutKC9]z9~');
define('AUTH_SALT',        'OsBVlRY3.Y<c)FT:;nY%OW?b?O9S2/>#Fm:$|=z ,u2{p,${>xs[4-34#fHophk!');
define('SECURE_AUTH_SALT', '$%]CG1O(*jwFqtOB{(B8V5WEI|5})AQ7534y$9-~c/r+2(&-=8atP8[:!*DK|**4');
define('LOGGED_IN_SALT',   'jIN;QHx]ph6[`)wF=Fw:gE7O=jW=.+/f]I3B-%{!;CZ04B,3l_d7OadYW:8AwQ*r');
define('NONCE_SALT',       '}y5X:=8f087o%ot3s0$1!pRTi3AyKrky/4A,|}xn 8I)FYZYKi(`TS.7@Su<d&[~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'robus_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
