<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Barcode,Printer,Bangladesh</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="author" content="Robust"/>
        <meta name="description" content="Barcode,Printer,Bangladesh,Barcode,Labels,Bangladesh,Barcode,Ribbons,Bangladesh,Barcode,Scanner,Bangladesh,Blitz,2253,Numbering,Machine,Bangladesh,22x12,Numbering,Sticker,Bangladesh"/>
        <meta name="keywords" content="Barcode,Printer,Bangladesh,Barcode,Labels,Bangladesh,Barcode,Ribbons,Bangladesh,Barcode,Scanner,Bangladesh,Blitz,2253,Numbering,Machine,Bangladesh,22x12,Numbering,Sticker,Bangladesh"/>

        <!-- Bootstrap core CSS -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <link href="resources/css/common.css" rel="stylesheet"/>
        <link href="resources/css/style.css" rel="stylesheet"/>
        <link href="resources/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="resources/css/themify-icons.css">

        <!-- Page level plugin styles START -->
        <link href="resources/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
        <link href="resources/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="resources/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
        <!-- Page level plugin styles END -->

        <!-- Theme styles START -->
        <link href="resources/frontend/css/components.css" rel="stylesheet">
        <link href="resources/frontend/css/style.css" rel="stylesheet">
        <link href="resources/frontend/css/style-revolution-slider.css" rel="stylesheet"><!-- metronic revo slider styles -->
        <link href="resources/frontend/css/style-responsive.css" rel="stylesheet">
        <link href="resources/frontend/css/red.css" rel="stylesheet" id="style-color">
        <link href="resources/frontend/css/custom.css" rel="stylesheet">
        <!-- Theme styles END -->

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>

        <script type="text/javascript">
            var BASEURL = "http://localhost/robustbd/html";
            var IMAGEPATH = "http://localhost/robustbd/html/resources/images/";
        </script>
    </head>
    <body class="inner-page inner-page-container">
        <!-- BEGIN TOP BAR -->
        <div class="pre-header">
            <div class="container">
                <div class="row">
                    <!-- BEGIN TOP BAR LEFT PART -->
                    <div class="col-md-6 col-sm-6 additional-shop-info">
                        <ul class="list-unstyled list-inline">
                            <li><i class="fa fa-phone"></i><span>+8801712312321</span></li>
                            <li><i class="fa fa-envelope"></i><span>info@robustbd.com</span></li>
                        </ul>
                    </div>
                    <!-- END TOP BAR LEFT PART -->
                    <!-- BEGIN TOP BAR MENU -->
                    <div class="col-md-6 col-sm-6 additional-nav">
                        <ul class="list-unstyled list-inline pull-right">
                            <li><a href="#">Log In</a></li>
                        </ul>
                    </div>
                    <!-- END TOP BAR MENU -->
                </div>
            </div>
        </div>
        <!-- END TOP BAR -->

        <!-- BEGIN NAVIGATION -->
        <div class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light no-bg">
                <div class="container">
                    <a class="navbar-brand" href="#"><img src="resources/images/robust-logo.png" alt="Robust International Limited"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Product</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    About
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Company Profile</a>
                                    <a class="dropdown-item" href="#">Our Mission</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Our Services</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact Us</a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
        <!-- BEGIN NAVIGATION -->
        <div class="main content-container margin-top-60">
            <div class="container">
                <div class="row margin-bottom-40">
                    <!-- BEGIN CONTENT -->
                    <div class="col-md-12">
                        <h1>Contacts</h1>
                        <div class="content-page padding-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="maps" class="gmaps margin-bottom-40" style="height:400px;"></div>
                                </div>
                                <div class="col-md-9 col-sm-9">
                                    <h2 class="tll">Contact Form</h2>
                                    <p>Lorem ipsum dolor sit amet, Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat consectetuer adipiscing elit, sed diam nonummy nibh euismod tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

                                    <!-- BEGIN FORM-->
                                    <form action="#" role="form">
                                        <div class="form-group">
                                            <label for="contacts-name">Name</label>
                                            <input type="text" class="form-control" id="contacts-name">
                                        </div>
                                        <div class="form-group">
                                            <label for="contacts-email">Email</label>
                                            <input type="email" class="form-control" id="contacts-email">
                                        </div>
                                        <div class="form-group">
                                            <label for="contacts-message">Message</label>
                                            <textarea class="form-control" rows="5" id="contacts-message"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Send</button>
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </form>
                                    <!-- END FORM-->
                                </div>

                                <div class="col-md-3 col-sm-3 sidebar2">
                                    <h2 class="tll">Our Contacts</h2>
                                    <address>
                                        <strong>ROBUST INTERNATIONAL LIMITED</strong><br>
                                        House No. 01,Road No.9B<br>
                                        Sector No.07, Uttara, Dhaka-1230<br>
                                        Bangladesh.<br>
                                        <abbr title="Phone">P:</abbr> +88 02 58954291, +88 02 58954307
                                    </address>
                                    <address>
                                        <strong>Email</strong><br>
                                        <a href="mailto:info@robustbd.com">info@robustbd.com</a><br>
                                        <a href="mailto:sales@robustbd.com">sales@robustbd.com</a>
                                    </address>
                                    <ul class="social-icons margin-bottom-40">
                                        <li><a href="#" data-original-title="facebook" class="facebook"></a></li>
                                        <li><a href="#" data-original-title="github" class="github"></a></li>
                                        <li><a href="#" data-original-title="Goole Plus" class="googleplus"></a></li>
                                        <li><a href="#" data-original-title="linkedin" class="linkedin"></a></li>
                                        <li><a href="#" data-original-title="rss" class="rss"></a></li>
                                    </ul>

                                    <h2 class="padding-top-30 tll">About Us</h2>
                                    <p>Sediam nonummy nibh euismod tation ullamcorper suscipit</p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-check"></i> Officia deserunt molliti</li>
                                        <li><i class="fa fa-check"></i> Consectetur adipiscing</li>
                                        <li><i class="fa fa-check"></i> Deserunt fpicia</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT -->
                </div>
            </div>
        </div>

        <!-- BEGIN FOOTER -->
        <footer class="black-bg">
            <div id="footer-nav" class="site-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-9">
                            <div id="footer-socials">
                                <div class="social-menu">
                                    <a href="#" target="_blank" title="Facebook"><span class="ti-facebook"></span></a>
                                    <a href="#" target="_blank" title="Twitter"><span class="ti-twitter-alt"></span></a>
                                    <a href="#" target="_blank" title="Linkedin"><span class="ti-linkedin"></span></a>
                                </div>
                            </div><!-- #footer-socials -->
                        </div>

                        <div class="col-md-6 col-md-pull-9 tlr">
                            <div class="footer-site-info tlr"><?php echo date('Y'); ?> &copy; Copyrights <a href="#" target="_self">Robust International Limited</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END FOOTER -->

        <!--[if lt IE 9]>
        <script src="/resources/js/respond.min.js"></script>
        <![endif]-->

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="resources/js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="resources/plugins/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="resources/plugins/fancybox/source/jquery.fancybox.pack.js"></script><!-- pop up -->
        <script src="resources/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

        <!-- BEGIN RevolutionSlider -->
        <script src="resources/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="resources/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="resources/frontend/scripts/revo-slider-init.js" type="text/javascript"></script>
        <!-- END RevolutionSlider -->

        <script src="resources/layout/js/jquery.easing.js"></script>
        <script src="resources/layout/js/jquery.parallax.js"></script>
        <script src="resources/layout/js/jquery.scrollTo.min.js"></script>

        <script src="resources/layout/js/layout.js" type="text/javascript"></script>
        <!--Maps JS-->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
        <script src="resources/js/maps.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                Layout.init();
                Layout.initOWL();
                RevosliderInit.initRevoSlider();
                Layout.initTwitter();
            });
        </script>
    </body>
</html>