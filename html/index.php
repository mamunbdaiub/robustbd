<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Barcode,Printer,Bangladesh</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="author" content="Robust"/>
        <meta name="description" content="Barcode,Printer,Bangladesh,Barcode,Labels,Bangladesh,Barcode,Ribbons,Bangladesh,Barcode,Scanner,Bangladesh,Blitz,2253,Numbering,Machine,Bangladesh,22x12,Numbering,Sticker,Bangladesh"/>
        <meta name="keywords" content="Barcode,Printer,Bangladesh,Barcode,Labels,Bangladesh,Barcode,Ribbons,Bangladesh,Barcode,Scanner,Bangladesh,Blitz,2253,Numbering,Machine,Bangladesh,22x12,Numbering,Sticker,Bangladesh"/>

        <!-- Bootstrap core CSS -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

        <link href="resources/css/common.css" rel="stylesheet"/>
        <link href="resources/css/style.css" rel="stylesheet"/>
        <link href="resources/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="resources/css/themify-icons.css">

        <!-- Page level plugin styles START -->
        <link href="resources/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
        <link href="resources/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="resources/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
        <!-- Page level plugin styles END -->

        <!-- Theme styles START -->
        <link href="resources/frontend/css/components.css" rel="stylesheet">
        <link href="resources/frontend/css/style.css" rel="stylesheet">
        <link href="resources/frontend/css/style-revolution-slider.css" rel="stylesheet"><!-- metronic revo slider styles -->
        <link href="resources/frontend/css/style-responsive.css" rel="stylesheet">
        <link href="resources/frontend/css/red.css" rel="stylesheet" id="style-color">
        <link href="resources/frontend/css/custom.css" rel="stylesheet">
        <!-- Theme styles END -->

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>

        <script type="text/javascript">
            var BASEURL = "http://localhost/robustbd/html";
            var IMAGEPATH = "http://localhost/robustbd/html/resources/images/";
        </script>
    </head>
    <body>
        <!-- BEGIN TOP BAR -->
        <div class="pre-header">
            <div class="container">
                <div class="row">
                    <!-- BEGIN TOP BAR LEFT PART -->
                    <div class="col-md-6 col-sm-6 additional-shop-info">
                        <ul class="list-unstyled list-inline">
                            <li><i class="fa fa-phone"></i><span>+8801712312321</span></li>
                            <li><i class="fa fa-envelope"></i><span>info@robustbd.com</span></li>
                        </ul>
                    </div>
                    <!-- END TOP BAR LEFT PART -->
                    <!-- BEGIN TOP BAR MENU -->
                    <div class="col-md-6 col-sm-6 additional-nav">
                        <ul class="list-unstyled list-inline pull-right">
                            <li><a href="#">Log In</a></li>
                        </ul>
                    </div>
                    <!-- END TOP BAR MENU -->
                </div>
            </div>
        </div>
        <!-- END TOP BAR -->

        <!-- BEGIN NAVIGATION -->
        <div class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light no-bg">
                <div class="container">
                    <a class="navbar-brand" href="#"><img src="resources/images/robust-logo.png" alt="Robust International Limited"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Product</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    About
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Company Profile</a>
                                    <a class="dropdown-item" href="#">Our Mission</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Our Services</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact Us</a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
        <!-- BEGIN NAVIGATION -->

        <!-- BEGIN SLIDER -->
        <div class="page-slider">
            <div class="fullwidthbanner-container revolution-slider margin-bottom-0">
                <div class="fullwidthabnner">
                    <ul id="revolutionul">
                        <!-- THE NEW SLIDE -->
                        <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="resources/frontend/img/revolutionslider/thumbs/thumb1.jpg">
                            <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                            <img src="resources/frontend/img/revolutionslider/bg1.jpg" alt="">

                            <div class="caption lft slide_title slide_item_left"
                                 data-x="30"
                                 data-y="90"
                                 data-speed="400"
                                 data-start="1500"
                                 data-easing="easeOutExpo">
                                Explore the Power<br><span class="slide_title_white_bold">of Metronic</span>
                            </div>
                            <div class="caption lft slide_subtitle_white slide_item_left slide_subtitle"
                                 data-x="87"
                                 data-y="245"
                                 data-speed="400"
                                 data-start="2000"
                                 data-easing="easeOutExpo">
                                This is what you were looking for
                            </div>
                            <a class="caption lft btn dark slide_btn slide_item_left" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
                               data-x="187"
                               data-y="315"
                               data-speed="400"
                               data-start="3000"
                               data-easing="easeOutExpo">
                                Purchase Now!
                            </a>
                            <div class="caption lfb"
                                 data-x="640"
                                 data-y="0"
                                 data-speed="700"
                                 data-start="1000"
                                 data-easing="easeOutExpo">
                                <img src="resources/frontend/img/revolutionslider/banner1.png" alt="Image 1">
                            </div>
                        </li>

                        <!-- THE FIRST SLIDE -->
                        <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="resources/frontend/img/revolutionslider/thumbs/thumb2.jpg">
                            <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                            <img src="resources/frontend/img/revolutionslider/bg1.jpg" alt="">

                            <div class="caption lft slide_title slide_item_left"
                                 data-x="30"
                                 data-y="105"
                                 data-speed="400"
                                 data-start="1500"
                                 data-easing="easeOutExpo">
                                Need a Printer?
                            </div>
                            <div class="caption lft slide_subtitle slide_item_left"
                                 data-x="30"
                                 data-y="180"
                                 data-speed="400"
                                 data-start="2000"
                                 data-easing="easeOutExpo">
                                This is what you were looking for
                            </div>
                            <div class="caption lft slide_desc slide_item_left"
                                 data-x="30"
                                 data-y="220"
                                 data-speed="400"
                                 data-start="2500"
                                 data-easing="easeOutExpo">
                                Lorem ipsum dolor sit amet, dolore eiusmod<br> quis tempor incididunt. Sed unde omnis iste.
                            </div>
                            <a class="caption lft btn green slide_btn slide_item_left" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
                               data-x="30"
                               data-y="290"
                               data-speed="400"
                               data-start="3000"
                               data-easing="easeOutExpo">
                                Purchase Now!
                            </a>
                            <div class="caption lfb"
                                 data-x="640"
                                 data-y="55"
                                 data-speed="700"
                                 data-start="1000"
                                 data-easing="easeOutExpo">
                                <img src="resources/frontend/img/revolutionslider/printer-animation.gif" alt="Image 1">
                            </div>
                        </li>

                        <!-- THE SECOND SLIDE -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400" data-thumb="resources/frontend/img/revolutionslider/thumbs/thumb2.jpg">
                            <img src="resources/frontend/img/revolutionslider/bg2.jpg" alt="">
                            <div class="caption lfl slide_title slide_item_left"
                                 data-x="30"
                                 data-y="125"
                                 data-speed="400"
                                 data-start="3500"
                                 data-easing="easeOutExpo">
                                Powerfull &amp; Clean
                            </div>
                            <div class="caption lfl slide_subtitle slide_item_left"
                                 data-x="30"
                                 data-y="200"
                                 data-speed="400"
                                 data-start="4000"
                                 data-easing="easeOutExpo">
                                Responsive Admin &amp; Website Theme
                            </div>
                            <div class="caption lfl slide_desc slide_item_left"
                                 data-x="30"
                                 data-y="245"
                                 data-speed="400"
                                 data-start="4500"
                                 data-easing="easeOutExpo">
                                Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                            </div>
                            <div class="caption lfr slide_item_right"
                                 data-x="635"
                                 data-y="105"
                                 data-speed="1200"
                                 data-start="1500"
                                 data-easing="easeOutBack">
                                <img src="resources/frontend/img/revolutionslider/barcode_paper.png" alt="Image 1">
                            </div>
                            <div class="caption lfr slide_item_right"
                                 data-x="580"
                                 data-y="245"
                                 data-speed="1200"
                                 data-start="2000"
                                 data-easing="easeOutBack">
                                <img src="resources/frontend/img/revolutionslider/paper_1.png" alt="Image 1">
                            </div>
                            <div class="caption lfr slide_item_right"
                                 data-x="835"
                                 data-y="230"
                                 data-speed="1200"
                                 data-start="3000"
                                 data-easing="easeOutBack">
                                <img src="resources/frontend/img/revolutionslider/paper_2.png" alt="Image 1">
                            </div>
                        </li>

                        <!-- THE THIRD SLIDE -->
                        <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="resources/frontend/img/revolutionslider/thumbs/thumb2.jpg">
                            <img src="resources/frontend/img/revolutionslider/bg3.jpg" alt="">
                            <div class="caption lfl slide_item_left"
                                 data-x="30"
                                 data-y="95"
                                 data-speed="400"
                                 data-start="1500"
                                 data-easing="easeOutBack">
                                <img src="resources/frontend/img/revolutionslider/Banner-2-printer.png" alt="Image 1">
                            </div>
                            <div class="caption lfr slide_title"
                                 data-x="470"
                                 data-y="100"
                                 data-speed="400"
                                 data-start="2000"
                                 data-easing="easeOutExpo">
                                Responsive Video Support
                            </div>
                            <div class="caption lfr slide_subtitle"
                                 data-x="470"
                                 data-y="170"
                                 data-speed="400"
                                 data-start="2500"
                                 data-easing="easeOutExpo">
                                Youtube, Vimeo and others.
                            </div>
                            <div class="caption lfr slide_desc"
                                 data-x="470"
                                 data-y="220"
                                 data-speed="400"
                                 data-start="3000"
                                 data-easing="easeOutExpo">
                                Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                            </div>
                            <a class="caption lfr btn yellow slide_btn" href=""
                               data-x="470"
                               data-y="280"
                               data-speed="400"
                               data-start="3500"
                               data-easing="easeOutExpo">
                                Watch more Videos!
                            </a>
                        </li>

                        <!-- THE FORTH SLIDE -->
                        <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="resources/frontend/img/revolutionslider/thumbs/thumb2.jpg">
                            <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                            <img src="resources/frontend/img/revolutionslider/bg4.jpg" alt="">
                            <div class="caption lft slide_title"
                                 data-x="30"
                                 data-y="105"
                                 data-speed="400"
                                 data-start="1500"
                                 data-easing="easeOutExpo">
                                What else included ?
                            </div>
                            <div class="caption lft slide_subtitle"
                                 data-x="30"
                                 data-y="180"
                                 data-speed="400"
                                 data-start="2000"
                                 data-easing="easeOutExpo">
                                The Most Complete Admin Theme
                            </div>
                            <div class="caption lft slide_desc"
                                 data-x="30"
                                 data-y="225"
                                 data-speed="400"
                                 data-start="2500"
                                 data-easing="easeOutExpo">
                                Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                            </div>
                            <a class="caption lft slide_btn btn red slide_item_left" href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin" target="_blank"
                               data-x="30"
                               data-y="300"
                               data-speed="400"
                               data-start="3000"
                               data-easing="easeOutExpo">
                                Learn More!
                            </a>
                            <div class="caption lft start"
                                 data-x="670"
                                 data-y="55"
                                 data-speed="400"
                                 data-start="2000"
                                 data-easing="easeOutBack">
                                <img src="resources/frontend/img/revolutionslider/scanner_1.png" alt="Image 2">
                            </div>

                            <div class="caption lft start"
                                 data-x="850"
                                 data-y="55"
                                 data-speed="400"
                                 data-start="2400"
                                 data-easing="easeOutBack">
                                <img src="resources/frontend/img/revolutionslider/scanner_2.png" alt="Image 3">
                            </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer tp-bottom"></div>
                </div>
            </div>
        </div>
        <!-- END SLIDER -->

        <!-- Services block BEGIN -->
        <div class="services-block content tlc bg-gray" id="services">
            <div class="container">
                <h2>Our <strong>services</strong></h2>
                <h4>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</h4>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 item">
                        <i class="fa fa-heart"></i>
                        <h3>Barcode Scanner</h3>
                        <p>Lorem ipsum et dolor amet<br> consectetuer diam</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 item">
                        <i class="fa fa-mobile"></i>
                        <h3>Barcode Labels</h3>
                        <p>Lorem ipsum et dolor amet<br> consectetuer diam</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 item">
                        <i class="fa fa-signal"></i>
                        <h3>Barcode Printer</h3>
                        <p>Lorem ipsum et dolor amet<br> consectetuer diam</p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 item">
                        <i class="fa fa-camera"></i>
                        <h3>Marking Labeler</h3>
                        <p>Lorem ipsum et dolor amet<br> consectetuer diam</p>
                    </div>
                </div>

                <div class="link tlc">
                    <a href="#" class="btn btn-link no-round text-uppercase">View All Services</a>
                </div>
            </div>
        </div>
        <!-- Services block END -->

        <!-- Product block BEGIN -->
        <div class="product-block content bg-white tlc padding-bottom-60" id="product">
            <div class="container">
                <div class="container">
                    <h2 class="margin-bottom-50">Our <strong>products</strong></h2>
                </div>
                <div class="row">
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/2.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/2.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 1</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/6.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/6.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 2</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/8.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/8.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 3</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/3.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/3.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 4</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/5.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/5.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 5</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/4.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/4.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 6</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/1.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/1.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 7</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/10.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/10.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 8</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/9.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/9.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 9</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/7.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/7.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 10</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/2.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/2.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 11</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                    <div class="item col-md-3 col-sm-6 col-xs-12">
                        <img src="resources/frontend/img/portfolio/8.jpg" alt="NAME" class="img-responsive">
                        <a href="resources/frontend/img/portfolio/8.jpg" class="zoom valign-center">
                            <div class="valign-center-elem">
                                <strong>Product 12</strong>
                                <em>Property</em>
                                <b>Details</b>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="link tlc">
                    <a href="#" class="btn btn-link no-round text-uppercase">View All Product</a>
                </div>
            </div>
        </div>
        <!-- Product block END -->

        <!-- Choose us block BEGIN -->
        <div class="choose-us-block content text-center margin-bottom-40 bg-gray" id="benefits">
            <div class="container">
                <h2>Why to <strong>choose us</strong></h2>
                <h4>Lorem ipsum dolor sit amet, <a href="javascript:void(0);">consectetuer adipiscing</a> elit, sed diam nonummy<br> nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h4>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <img src="resources/images/why_choose_us.png" alt="Why to choose us" class="img-responsive">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <div class="panel-group" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1">Lorem ipsum dolor sit amet</a>
                                    </h5>
                                </div>
                                <div id="accordion1_1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">Consectetur adipisicing elit</a>
                                    </h5>
                                </div>
                                <div id="accordion1_2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3">Augue assum anteposuerit dolore</a>
                                    </h5>
                                </div>
                                <div id="accordion1_3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4">Sollemnes in futurum</a>
                                    </h5>
                                </div>
                                <div id="accordion1_4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5">Nostrud Tempor veniam</a>
                                    </h5>
                                </div>
                                <div id="accordion1_5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_6">Ut enem magana sed dolore</a>
                                    </h5>
                                </div>
                                <div id="accordion1_6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation dolore magna ullamco.</p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco sed eiusmod tempor ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Choose us block END -->

        <!-- BEGIN CLIENTS -->
        <div class="our-clients bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h2 class="tll"><a href="#">Our Clients</a></h2>
                        <p>Lorem dipsum folor margade sitede lametep eiusmod psumquis dolore.</p>
                    </div>
                    <div class="col-md-9">
                        <div class="owl-carousel owl-carousel6-brands">
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_1_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_1.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_2_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_2.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_3_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_3.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_4_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_4.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_5_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_5.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_6_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_6.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_7_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_7.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#">
                                    <img src="resources/frontend/img/clients/client_8_gray.png" class="img-responsive" alt="">
                                    <img src="resources/frontend/img/clients/client_8.png" class="color-img img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="link tlc">
                    <a href="#" class="btn btn-link no-round text-uppercase">View All Client</a>
                </div>
            </div>
        </div>
        <!-- END CLIENTS -->

        <!--Map-->
        <div id="maps"></div>
        <!--Map/-->

        <!-- BEGIN FOOTER -->
        <footer class="black-bg">
            <div class="container">
                <div class="row">
                    <div class="offset-top">
                        <div class="col-xs-12 col-md-12 col-lg-12 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="well well-lg">
                                <h3>Get in Touch</h3>
                                <div class="space-20"></div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label for="form-name" class="sr-only">Name</label>
                                            <input type="text" name="name" id="form-name" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="space-10"></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label for="form-email" class="sr-only">Email</label>
                                            <input type="text" name="email" id="form-email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="space-10"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="form-subject" class="sr-only">Subject</label>
                                            <input type="text" name="subject" class="form-control" id="form-subject" placeholder="Subject">
                                        </div>
                                        <div class="space-10"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="form-message" class="sr-only">comment</label>
                                            <textarea name="comment" id="form-message" class="form-control" placeholder="Message" cols="20" rows="6"></textarea>
                                        </div>
                                        <div class="space-10"></div>
                                        <input type="submit" value="Send message" class="btn btn-link no-round text-uppercase">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="footer-nav" class="site-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-9">
                            <div id="footer-socials">
                                <div class="social-menu">
                                    <a href="#" target="_blank" title="Facebook"><span class="ti-facebook"></span></a>
                                    <a href="#" target="_blank" title="Twitter"><span class="ti-twitter-alt"></span></a>
                                    <a href="#" target="_blank" title="Linkedin"><span class="ti-linkedin"></span></a>
                                </div>
                            </div><!-- #footer-socials -->
                        </div>

                        <div class="col-md-6 col-md-pull-9 tlr">
                            <div class="footer-site-info tlr"><?php echo date('Y'); ?> &copy; Copyrights <a href="#" target="_self">Robust International Limited</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END FOOTER -->

        <!--[if lt IE 9]>
        <script src="/resources/js/respond.min.js"></script>
        <![endif]-->

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="resources/js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="resources/plugins/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="resources/plugins/fancybox/source/jquery.fancybox.pack.js"></script><!-- pop up -->
        <script src="resources/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

        <!-- BEGIN RevolutionSlider -->
        <script src="resources/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="resources/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="resources/frontend/scripts/revo-slider-init.js" type="text/javascript"></script>
        <!-- END RevolutionSlider -->

        <script src="resources/layout/js/jquery.easing.js"></script>
        <script src="resources/layout/js/jquery.parallax.js"></script>
        <script src="resources/layout/js/jquery.scrollTo.min.js"></script>

        <script src="resources/layout/js/layout.js" type="text/javascript"></script>
        <!--Maps JS-->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
        <script src="resources/js/maps.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                Layout.init();
                Layout.initOWL();
                RevosliderInit.initRevoSlider();
                Layout.initTwitter();
            });
        </script>
    </body>
</html>